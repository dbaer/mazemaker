#ifndef _OPTIONS_H
#define _OPTIONS_H 1

#include <stdint.h>

typedef uint8_t rgb_color_t[3];

typedef struct mazeoptions {
    rgb_color_t wall_color;
    rgb_color_t background_color;
} mazeoptions_t;

#endif // !def(_OPTIONS_H)
