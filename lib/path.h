#ifndef _PATH_H
#define _PATH_H 1

#include "grid.h"

typedef struct {
    unsigned row, col;
} block_t;

typedef struct {
    size_t length;
    block_t *nodes;
} path_t;

int shortest_path(mazegrid_t const* maze, path_t* result);
void free_path(path_t* p);

#endif // !def(_PATH_H)
