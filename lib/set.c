#include <stdlib.h>
#include "set.h"

struct node_set {
    size_t width, height;
    size_t size;
    bool** nodes;
};

struct node_set* node_set_new(size_t width, size_t height) {
    struct node_set* result = malloc(sizeof(struct node_set));
    result->nodes = calloc(height, sizeof(bool*));
    for (size_t i = 0; i < height; i++) {
        result->nodes[i] = calloc(width, sizeof(bool));
        for (size_t j = 0; j < width; j++) {
            result->nodes[i][j] = false;
        }
    }
    result->width = width;
    result->height = height;
    result->size = 0;
    return result;
}

void node_set_add(struct node_set* s, size_t x, size_t y) {
    s->nodes[y][x] = true;
    s->size++;
}

bool node_set_contains(struct node_set const* s, size_t x, size_t y) {
    return s->nodes[y][x];
}

void node_set_free(struct node_set* s) {
    for (size_t i = 0; i < s->height; i++) {
        free(s->nodes[i]);
        s->nodes[i] = NULL;
    }
    free(s->nodes);
    s->nodes = NULL;
    s->width = s->height = s->size = 0;
    free(s);
}

size_t node_set_size(struct node_set const* s) {
    return s->size;
}
