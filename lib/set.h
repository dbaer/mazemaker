#ifndef _SET_H
#define _SET_H 1

#include <stdbool.h>
#include "grid.h"

struct node_set;

struct node_set* node_set_new(size_t w, size_t h);
void node_set_add(struct node_set* s, size_t x, size_t y);
bool node_set_contains(struct node_set const* s, size_t x, size_t y);
size_t node_set_size(struct node_set const* s);
void node_set_free(struct node_set*);

#endif // !def(_SET_H)
