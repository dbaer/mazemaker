#ifndef _PQ_H
#define _PQ_H 1

#include <stdbool.h>
#include "grid.h"

struct pq;

struct pq* pq_new(size_t heap_size);
void pq_add(struct pq* q, unsigned int priority, size_t x, size_t y, mazeedge_dir_t dir);
int pq_pop(struct pq* q, size_t* x, size_t* y, mazeedge_dir_t *dir);
bool pq_empty(struct pq const* q);
void pq_free(struct pq* q);

#endif // !def(_PQ_H)
