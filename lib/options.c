#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "options.h"

mazeoptions_t* mazemaker_options_new() {
    mazeoptions_t* result = malloc(sizeof(mazeoptions_t));

    // set defaults
    memset(result->wall_color, 0, 3);
    memset(result->background_color, 0xff, 3);

    return result;
}

void mazemaker_options_free(mazeoptions_t* options) {
    free(options);
}

static int interpretColorName(char const* color_desc, rgb_color_t dst) {
    if (0 == strcasecmp(color_desc, "black")) { dst[0] = dst[1] = dst[2] = 0; }
    else if (0 == strcasecmp(color_desc, "white")) { dst[0] = dst[1] = dst[2] = 0xff; }
    else if (0 == strcasecmp(color_desc, "red")) { dst[0] = 0xff; dst[1] = dst[2] = 0; }
    else if (0 == strcasecmp(color_desc, "blue")) { dst[0] = dst[1] = 0; dst[2] = 0xff; }
    else if (0 == strcasecmp(color_desc, "green")) { dst[0] = dst[2] = 0; dst[1] = 0xff; }
    else if (0 == strcasecmp(color_desc, "yellow")) { dst[2] = 0; dst[0] = dst[1] = 0xff; }
    else if ((0 == strcasecmp(color_desc, "magenta")) ||
             (0 == strcasecmp(color_desc, "purple"))) { dst[1] = 0; dst[0] = dst[2] = 0xff; }
    else if (0 == strcasecmp(color_desc, "cyan")) { dst[0] = 0; dst[1] = dst[2] = 0xff; }
    else if (0 == strcasecmp(color_desc, "orange")) { dst[0] = 0xff; dst[1] = 0x7f; dst[2] = 0; }
    else if (0 == strcasecmp(color_desc, "brown")) { dst[0] = 0x7f; dst[1] = 0x3f; dst[2] = 0; }
    else if ((0 == strcasecmp(color_desc, "gray")) ||
             (0 == strcasecmp(color_desc, "grey"))) { dst[0] = dst[1] = dst[2] = 0x7f; }
    else if (0 == strcasecmp(color_desc, "darkblue")) { dst[0] = dst[1] = 0; dst[2] = 0x7f; }
    else if (0 == strcasecmp(color_desc, "darkgreen")) { dst[0] = dst[2] = 0; dst[1] = 0x7f; }
    else if (0 == strcasecmp(color_desc, "darkred")) { dst[0] = 0x7f; dst[1] = dst[2] = 0; }
    else if (0 == strcasecmp(color_desc, "pink")) { dst[0] = 0xff; dst[1] = 0xa0; dst[2] = 0xbf; }
    else if ((0 == strcasecmp(color_desc, "darkmagenta")) ||
             (0 == strcasecmp(color_desc, "darkpurple"))) { dst[1] = 0; dst[0] = dst[2] = 0x7f; }
    else if (0 == strcasecmp(color_desc, "darkcyan")) { dst[0] = 0; dst[1] = dst[2] = 0x7f; }
    else return -1;
    return 0;
}

static int stringToColor(char const* color_desc, rgb_color_t dst) {
    if (color_desc[0] != '#') return interpretColorName(color_desc, dst); 

    size_t l;
    for (l = 1; color_desc[l] != '\0'; l++) if (!isxdigit(color_desc[l])) return -1; // bad format

    if ((l != 4) && (l != 7)) return -1; // bad format

    char x2[3] = { 0, 0, 0 };
    if (l == 4) {
        x2[0] = x2[1] = color_desc[1];
        sscanf(x2, "%hhx", &dst[0]);
        x2[0] = x2[1] = color_desc[2];
        sscanf(x2, "%hhx", &dst[1]);
        x2[0] = x2[1] = color_desc[3];
        sscanf(x2, "%hhx", &dst[2]);
    } else {
        strncpy(x2, color_desc + 1, 2);
        sscanf(x2, "%hhx", &dst[0]);
        strncpy(x2, color_desc + 3, 2);
        sscanf(x2, "%hhx", &dst[1]);
        strncpy(x2, color_desc + 5, 2);
        sscanf(x2, "%hhx", &dst[2]);
    }
    return 0;
}

int mazemaker_options_set_wall_color(mazeoptions_t* options, char const* color_desc) {
    return stringToColor(color_desc, options->wall_color);
}

int mazemaker_options_set_background_color(mazeoptions_t* options, char const* color_desc) {
    return stringToColor(color_desc, options->background_color);
}

