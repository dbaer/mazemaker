#ifndef _PRIM_H
#define _PRIM_H 1

#include "grid.h"

void prim(mazegrid_t const* grid, mazegrid_t* result);

#endif // !def(_PRIM_H)
