#include <mazemaker.h>
#include "grid.h"
#include "prim.h"

void mazemaker_generate_maze(int width, int height, mazegrid_t* result) {
    mazegrid_t g = mazegrid_new(width, height);
    mazegrid_randomize(&g);
    prim(&g, result);
    mazegrid_free(&g);
}
void mazemaker_free_maze(mazegrid_t* maze) {
    mazegrid_free(maze);
}
