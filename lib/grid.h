#ifndef _GRID_H
#define _GRID_H 1

#include <mazemaker.h>
#include <stdio.h>

mazegrid_t mazegrid_new(size_t width, size_t height);
void mazegrid_free(mazegrid_t* g);

int mazegrid_set_edge(mazegrid_t* g, size_t x, size_t y, mazeedge_dir_t dir, edgeweight_t wt);
edgeweight_t mazegrid_get_edge(mazegrid_t const* g, size_t, size_t y, mazeedge_dir_t dir);

void mazegrid_randomize(mazegrid_t* g);
void mazegrid_uniform(mazegrid_t* g);

void mazegrid_print(mazegrid_t const* g, FILE * f);

#endif // !def(_GRID_H)
