#ifndef _MAZEMAKER_H
#define _MAZEMAKER_H 1

#include <stdlib.h>
#include <stdint.h>

typedef uint8_t edgeweight_t;

typedef struct {
    edgeweight_t up, right;
} mazeedges_t;

typedef struct {
    size_t width, height;
    mazeedges_t** grid;
} mazegrid_t;

typedef enum {
    EDGE_UP,
    EDGE_RIGHT,
    EDGE_DOWN,
    EDGE_LEFT
} mazeedge_dir_t;

typedef struct mazeoptions mazeoptions_t;

void mazemaker_generate_maze(int width, int height, mazegrid_t* result);
void mazemaker_free_maze(mazegrid_t* maze);
int mazemaker_maze_to_png(mazegrid_t const* maze, char const* filename);
int mazemaker_maze_to_png_opt(mazegrid_t const* maze, char const* filename, mazeoptions_t const*);
int mazemaker_maze_to_png_mem(mazegrid_t const* maze, size_t* len, uint8_t** buf);
int mazemaker_maze_to_png_mem_opt(mazegrid_t const* maze, size_t* len, uint8_t** buf, mazeoptions_t const*);

mazeoptions_t* mazemaker_options_new();
void mazemaker_options_free(mazeoptions_t*);
int mazemaker_options_set_wall_color(mazeoptions_t*, char const* color_desc);
int mazemaker_options_set_background_color(mazeoptions_t*, char const* color_desc);

#endif // !def(_MAZEMAKER_H)
