prefix=${CMAKE_INSTALL_PREFIX}
exec_prefix=${EXEC_INSTALL_PREFIX}
libdir=${LIB_INSTALL_DIR}
includedir=${INCLUDE_INSTALL_DIR}

Name: ${PROJECT_NAME}
Description: Mazemaker library
Version: ${PROJECT_VERSION}
Requires: libpng
Libs: -L${LIB_INSTALL_DIR} -lmazemaker
Cflags: -I${INCLUDE_INSTALL_DIR}
