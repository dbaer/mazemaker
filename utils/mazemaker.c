#include <stdio.h>
#include <stdlib.h>
#include <popt.h>
#include <mazemaker.h>

int main(int argc, char* argv[]) {
    char c;
    int width = 0, height = 0;
    char const *filename = NULL, *fg_color = NULL, *bg_color = NULL;
    struct poptOption options_table[] = {
        { "width", 'w', POPT_ARG_INT, &width, 0,
          "Width of the maze", "BLOCKS" },
        { "height", 'h', POPT_ARG_INT, &height, 0,
          "Height of the maze", "BLOCKS" },
        { "foreground", 'f', POPT_ARG_STRING, &fg_color, 0,
          "Foreground (wall) color", "#rrggbb" },
        { "background", 'b', POPT_ARG_STRING, &bg_color, 0,
          "Background color", "#rrggbb" },
        POPT_AUTOHELP
        { NULL, 0, 0, NULL, 0 }
    };
    poptContext ctx = poptGetContext(NULL, argc, (const char**) argv, options_table, 0);
    poptSetOtherOptionHelp(ctx, "OUTPUT");
    if (argc < 2) {
        poptPrintUsage(ctx, stderr, 0);
        exit(1);
    }
    while ((c = poptGetNextOpt(ctx)) >= 0) /* noop */ ;
    if (c < -1) {
        fprintf(stderr, "%s: %s\n", poptBadOption(ctx, POPT_BADOPTION_NOALIAS), poptStrerror(c));
        return 1;
    }
    filename = poptGetArg(ctx);
    if ((width == 0) || (height == 0)) {
        fprintf(stderr, "Positive values for width (-w) and height (-h) are required.\n");
        return 1;
    }
    if (filename == NULL) {
        fprintf(stderr, "An output filename is required.\n");
        return 1;
    }
    mazegrid_t maze;
    mazeoptions_t* options = mazemaker_options_new();
    if (fg_color != NULL) {
        if (0 > mazemaker_options_set_wall_color(options, fg_color)) {
            fprintf(stderr, "Unknown color: \"%s\"\n", fg_color);
            exit(1);
        }
    }
    if (bg_color != NULL) {
        if (0 > mazemaker_options_set_background_color(options, bg_color)) {
            fprintf(stderr, "Unknown color: \"%s\"\n", bg_color);
            exit(1);
        }
    }
    mazemaker_generate_maze(width, height, &maze);
    mazemaker_maze_to_png_opt(&maze, filename, options);
    mazemaker_free_maze(&maze);
    mazemaker_options_free(options);
    poptFreeContext(ctx);
}
